package info.fges.blablacool.webclient;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/auth/")
public class AuthController
{
    @RequestMapping(value="login", method = RequestMethod.GET)
    public String getLogin(ModelMap model)
    {
        model.addAttribute("message", "Hello world!");
        model.addAttribute("bc_title", "Se connecter");
        return "auth/login";
    }

    @RequestMapping(value="register", method = RequestMethod.GET)
    public String getRegister(ModelMap model)
    {
        model.addAttribute("message", "Hello world!");
        return "auth/register";
    }
}